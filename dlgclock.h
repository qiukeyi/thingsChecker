#ifndef DLGCLOCK_H
#define DLGCLOCK_H
#include <QLCDNumber>
#include <QTimer>
#include <QTime>
#include <QMouseEvent>
#include <QPalette>
#include <QColor>
#include <QPushButton>
#include <QMessageBox>
class DlgClock : public QLCDNumber
{
    Q_OBJECT
public:
    /**
     * @brief DlgClock 计时器构造函数
     * @param parent 父控件
     * @param restTime 剩余时间 最大不会超过86400
     * @param button 触发计时按钮
     */
    DlgClock(QWidget *parent=0,int restTime=0,QPushButton * button=0);
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent * event);
public slots:
    /**
     * @brief updateClock 更新当前时间
     */
    void updateClock();
private:
    QPoint dragPosition;//保存坐标
    int restTime;//剩余时间
    QPushButton * button;
    QTimer * timer;//计时器指针
    /**
     * @brief renderClock 对时钟颜色进行渲染
     */
    void renderClock();
};

#endif // DLGCLOCK_H
