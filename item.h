#ifndef ITEM_H
#define ITEM_H

#include <QWidget>
#include <QDateTime>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QMap>
#include "dlgclock.h"
#include "itemdao.h"
class Item : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Item 测试的时候调用的构造函数
     * @param parent 父窗口
     * @param content 内容
     * @param datetime 截止时间

    Item(QWidget *parent=0,QString content="测试内容",QDateTime datetime=QDateTime::currentDateTime());
    */
    /**
     * @brief Item 正式的构造函数
     * @param parent 父窗口
     * @param data 项目的数据结构体
     * @param listP 列表指针
     */
    Item(QWidget *parent=0,ItemDao::ItemData data={-1,"没有内容","没有时间"},QVBoxLayout * listP=NULL);
    /**
     * @brief getRestSecond 获取任务到当前时间剩余的秒数
     * @return
     */
    long getRestSecond();
    /**
     * @brief getItemId 获取项目id
     * @return
     */
    int getItemId();
    /**
     * @brief setCondition 根据状态值设置颜色
     * @param value 值
     */
    void setCondition(int value);
signals:

public slots:
        void pressCheck();//点击复选框
        void startClock();//计时按钮
private:
    QLabel * content;//内容标签
    QLabel * endTime;//截止时间标签
    QCheckBox * checkBox;//标记完成标签
    QPushButton * pushButton;//计时按钮
    QHBoxLayout * layout;//布局
    ItemDao::ItemData data;//数据结构体
    QVBoxLayout * listP;//列表指针
    QWidget * parentP;
};

#endif // ITEM_H
