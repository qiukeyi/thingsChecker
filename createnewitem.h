#ifndef CREATENEWITEM_H
#define CREATENEWITEM_H

#include <QLabel>
#include <QLineEdit>
#include <QDateTimeEdit>
#include <QGridLayout>
#include <QDialog>
#include <QPushButton>
#include <QHBoxLayout>
#include <QPalette>
#include <QEvent>
#include <QMouseEvent>
#include <QDateTime>
#include <QMessageBox>
#include <QVBoxLayout>
#include "item.h"
class CreateNewItem : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief CreateNewItem 打开一个创建新任务的窗口
     * @param parent 父窗口
     * @param listp 列表布局指针
     */
    CreateNewItem(QWidget *parent = 0,QVBoxLayout * listp=NULL);
    void mouseMoveEvent(QMouseEvent *event);//鼠标移动事件
    void mousePressEvent(QMouseEvent *event);//鼠标按压事件
    static bool flag;
public slots:
    void pressCancel();//点击取消按钮
    void pressOk();//点击确定按钮
private:
    QLabel * contentLabel;//内容标签
    QLabel * timeLabel;//时间标签
    QLineEdit * contentEdit;//内容输入框
    QDateTimeEdit * timeEdit;//时间输入框
    QGridLayout * layout;//布局
    QPushButton * enter;//确定
    QPushButton * cancel;//取消
    QPoint dragPosition;//保存鼠标点相对对话框窗体左上角的偏移值
    QVBoxLayout * listP;//外界指针
};

#endif // CREATENEWITEM_H
