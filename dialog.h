#ifndef DIALOG_H
#define DIALOG_H
#include <QPoint>
#include <QDialog>
#include "item.h"
#include <QVBoxLayout>
#include <QPalette>
#include <QEvent>
#include <QList>
#include <QMap>
#include <QTimer>
#include "item.h"
#include "itemdao.h"
#include "createnewitem.h"
class Dialog : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Dialog 构造函数
     * @param parent 父窗口
     */
    Dialog(QWidget *parent = 0);
    /**
     * @brief mouseMoveEvent 鼠标移动事件
     * @param event 事件
     */
    void mouseMoveEvent(QMouseEvent *event);
    /**
     * @brief mousePressEvent 鼠标按压事件
     * @param event 事件
     */
    void mousePressEvent(QMouseEvent *event);

public slots:
    bool eventFilter(QObject *, QEvent *);
    /**
     * @brief refreshDialog 刷新界面
     */
    void refreshDialog();

private:
    /**
     * @brief renderColor 根据情况染色
     * @param item 项目指针
     */
    void renderColor(Item * item);
    QPoint dragPosition;//保存鼠标点相对窗体左上角的偏移值
    QVBoxLayout * layout;//整体布局
    QVBoxLayout *list;//列表项布局
    QLabel * add;//添加新的内容

    QTimer * timer;//计时器
};

#endif // DIALOG_H
