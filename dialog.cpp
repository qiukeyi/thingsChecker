#include "dialog.h"
#include <QDateTime>
#include <QMouseEvent>
#include <QDebug>
bool CreateNewItem::flag=false;
Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
    QPalette p=palette();
    p.setColor(QPalette::Window,Qt::yellow);
    setPalette(p);
    setWindowOpacity(0.8);
    setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);
    add=new QLabel(tr("添加新的内容"));
    add->installEventFilter(this);
    layout=new QVBoxLayout(this);
    list=new QVBoxLayout();
    ItemDao itemDao;
    QList<ItemDao::ItemData> itemList=itemDao.queryItem();
    for(int i=0;i<itemList.count();i++)
    {
        Item * temp=new Item(this,itemList.value(i),list);
        qDebug()<<temp->getRestSecond();
        if(temp->getRestSecond()<=0)            //如果任务已经超时
        {
            itemDao.finishItem(temp->getItemId(),2);    //标记任务失败
            delete temp;                        //删除指针
        }
        else
        {
            //temp->setCondition(255);
            renderColor(temp);
            list->addWidget(temp);          //添加到列表中
        }

    }
    /*list->addWidget(new Item(this,tr("测试构造函数")));
    list->addWidget(new Item(this));
    list->addWidget(new Item(this,tr("测试构造函数"),QDateTime::currentDateTime()));*/

    layout->addLayout(list);
    layout->addWidget(add,0,Qt::AlignHCenter);
    timer=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(refreshDialog()));
    timer->start(5000);
}
bool Dialog::eventFilter(QObject * watched, QEvent * event)
{
    if(event->type()==QEvent::MouseButtonDblClick)
    {
        QMouseEvent *mouseEvent=(QMouseEvent*)event;
        if(mouseEvent->buttons()&Qt::LeftButton)
        {
            if(!CreateNewItem::flag)
            {
                CreateNewItem * newItemDialog=new CreateNewItem(NULL,list);
                newItemDialog->show();
                CreateNewItem::flag=true;
            }
        }

    }
    return QDialog::eventFilter(watched,event);
}


void Dialog::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
        dragPosition=event->globalPos()-frameGeometry().topLeft();
        event->accept();
    }
    if(event->button()==Qt::RightButton)
    {
        close();
    }
}
void Dialog::mouseMoveEvent(QMouseEvent *event)
{
    if(event->buttons()&Qt::LeftButton)
    {
        move(event->globalPos()-dragPosition);
        event->accept();
    }
}

void Dialog::refreshDialog()
{
    for(int i=0;i<list->count();i++)
    {
        Item * cur=(Item *)list->itemAt(i)->widget();
        renderColor(cur);
    }
    qDebug()<<"刷新界面";
}

void Dialog::renderColor(Item *item)
{
    int rest=item->getRestSecond();      //获取该项目剩余时间
    if(rest<=0)     //如果已经超时
    {
       qDebug()<<"删除超时项目";
       list->removeWidget(item);
       item->deleteLater();     //删除项目
    }
    if(rest<=3600)//如果不到一小时
    {
        item->setCondition(255-rest/30);
    }
    else if(rest<=7200)
    {
        rest-=3600;
        item->setCondition(135+rest/60);
    }
    else if(rest<=10800)
    {
        rest-=7200;
        item->setCondition(75+rest/120);
    }
    else if(rest<=14400)
    {
        rest-=10800;
        item->setCondition(45+rest/240);
    }
    else if(rest<18000)
    {
        rest-=14400;
        item->setCondition(30+rest/240);
    }
    else if(rest<21600)
    {
        rest-=18000;
        item->setCondition(15+rest/240);
    }
}
