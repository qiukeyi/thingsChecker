#ifndef ITEMDAO_H
#define ITEMDAO_H
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDateTime>
#include <QList>
class ItemDao
{
public:
    /**
     * @brief ItemDao 构造一个数据库操作实体
     * @param hostName 主机名称
     * @param dbName 数据库名称
     * @param username 用户名
     * @param password 密码
     * @param tableName 表名
     */
    ItemDao(QString hostName="z-terminal",QString dbName="timeChecker",QString username="zekdot",QString password="123456",QString tableName="itemTable");
    /**
     * @brief insertItem 插入一个新的事件
     * @param content 内容
     * @param endTime 结束事件
     * @return  插入后的rowid
     */
    int insertItem(QString content,QDateTime endTime);
    /**
     * @brief The ItemData struct 用于返回相关信息的结构体
     */
    struct ItemData{
        int id;//序号
        QString content;//内容
        QString dateTime;//截止时间
    };
    /**
     * @brief queryItem 查询所有任务
     * @return 返回任务数据的结构体
     */
    QList<ItemData> queryItem();
    /**
     * @brief finishItem 标记完成一条任务，可以指定结果，1表示成功完成，2表示超时
     * @param id 任务序号
     * @param result 结果
     * @return
     */
    bool finishItem(int id,int result);

private:
    QSqlDatabase db;//数据库实体
    QString tableName;//表名
};

#endif // ITEMDAO_H
